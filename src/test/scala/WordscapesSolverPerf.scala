import scala.io.Source
import org.scalameter.api._
import org.scalameter.picklers.Implicits._

object RangeBenchmark extends Bench.LocalTime {

  val sizes: Gen[Int] = Gen.range("size")(300000, 1500000, 300000)
  val ranges: Gen[Range] = for {
    size <- sizes
  } yield 0 until size
  val letters: Gen[String] = Gen.enumeration("letters")("cabk", "tkac", "legacy")
  val validWords = Source.fromResource("words").getLines.toList

  performance of "StringOps" in {
    measure method "enumerate" in {
      using(letters) in {
        l => WordscapesSolver.StringOps(l).enumerate
      }
    }
  }

  performance of "WordscapesSolver" in {
    measure method "solve" in {
      using(letters) in {
        l => WordscapesSolver.solve(l, validWords)
      }
    }
  }
}
