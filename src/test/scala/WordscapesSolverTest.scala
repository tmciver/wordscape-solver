import org.scalatest._
import scala.io.Source
import WordscapesSolver._

class WordscapesSolverTest extends FlatSpec {

  "Enumeration of the string \"abc\"" should "have the correct enumerations" in {
    val expected = Set("ab", "ac", "ba", "bc", "ca", "cb", "abc", "acb", "bac", "bca", "cab", "cba", "a", "b", "c")
    assert("abc".enumerate === expected)
  }

  "The solution of the letters \"speak\"" should "be" in {
    val words = Source.fromResource("words").getLines.toList
    assert(WordscapesSolver.solve("speak", words) === Set("pas", "sake", "sea", "asp", "spake", "pea", "peak", "apes", "apse", "peas", "ape", "peaks", "spa", "ask", "sap", "speak"))
  }
}
