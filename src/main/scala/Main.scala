package com.timmciver.wordscapessolver

import scala.io.StdIn
import scala.io.Source

import cats.effect._
import cats.implicits._

object Main extends IOApp {

  def putStrLn(value: String) = IO(println(value))
  def putStr(value: String) = IO(print(value))
  val readLn = IO(scala.io.StdIn.readLine)

  override def run(args: List[String]): IO[ExitCode] = for {
    _ <- putStr("Enter letters: ")
    letters <- readLn
    possibleWords <- WordscapesSolver.solve[IO](letters)
    solnStr = possibleWords.mkString(", ")
    _ <- putStrLn(s"Possible words: $solnStr")
  } yield ExitCode.Success
}
