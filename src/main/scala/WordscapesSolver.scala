package com.timmciver.wordscapessolver

import cats.Monad
import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.effect._
import scala.io.Source

object WordscapesSolver {

  implicit class StringOps(s: String) {
    def enumerate: Set[String] = {
      val l = s.toList
      val enums = for {
        n <- 1 to l.size
        comb <- l.combinations(n)
        perm <- comb.permutations
        permStr = perm.mkString("")
      } yield permStr

      enums.toSet
    }
  }

  private def getDictionary[F[_]: Monad]: F[Seq[String]] = for {
    cl <- implicitly[Monad[F]].pure(Main.getClass.getClassLoader) // Without this ClassLoader, the "words" file is not retrieved.
    res <- implicitly[Monad[F]].pure(Source.fromResource("words", cl))
    lines = res.getLines
  } yield lines.toSeq

  def solve(letters: String, validWords: Seq[String]): Set[String] =
    letters.enumerate
      .filter(_.length > 2)
      .filter(validWords.contains)

  def solve[F[_]: Effect](letters: String): F[Set[String]] = getDictionary.map(solve(letters, _))
}
